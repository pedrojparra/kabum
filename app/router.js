'use strict';

module.exports = function(app) {

  // Loading controllers
  var homeController = require('./controllers/homeController');
  var contactController = require('./controllers/contactController');
  var sitemapController = require('./controllers/sitemapController');

  // Text data for templates
  app.use(function(req, res, next) {
    res.locals.__ = require('./content/i18n/es.json');
    next();
  });

  // Controller dispatch
  app.use('/', homeController);
  app.use('/contacto', contactController);

  // Sitemap
  app.use('/sitemap.xml',sitemapController);

  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // error handlers

  // development error handler
  // will print stacktrace
  if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
      res.status(err.status || 500);
      res.render('error', {
        message: err.message,
        error: err
      });
    });
  }

  // production error handler
  // no stacktraces leaked to user
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: {}
    });
  });

};
