'use strict';

// Loading dependencies
var express = require('express');
var router = express.Router();

// Home Controller
router.get('/', function(req, res, next) {
  res.render('index', {

    seo: {
      title: 'title 2',
      description: 'description',
      keywords: 'keyword'
    },

    metaSocial: {
      title: '',
      image: '',
      url: '',
      description: ''
    }


  });
});

// Export
module.exports = router;
