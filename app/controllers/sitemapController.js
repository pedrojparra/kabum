'use strict';

// Loading dependencies
var express = require('express');
var router = express.Router();

// Sitemap Controller
router.get('/', function(req, res, next) {

  var rootPath = 'http://www.demo.es';
  var urls = [
    { url: '/', priority: 0.9 },
    { url: '/contacto', priority: 0.8 }
  ];

  var xml = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
  for ( var j = 0; j < urls.length; j++ ) {
      xml += '<url>';
      xml += '<loc>'+ rootPath + urls[j].url + '</loc>';
      xml += '<changefreq>monthly</changefreq>';
      xml += '<priority>'+ urls[j].priority +'</priority>';
      xml += '</url>';
  }
  xml += '</urlset>';
  res.header('Content-Type', 'text/xml');
  res.send(xml);

});

// Export
module.exports = router;
