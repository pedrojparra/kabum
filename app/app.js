'use strict';

// Loading dependencies
var express = require('express');
var path = require('path');

// Initializing express application
var app = express();

// Deploy Openshift
var cc = require('config-multipaas');
var deploy = cc().add({ IP: '127.0.0.1' });

// Body Parser
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Logger
var logger = require('morgan');
app.use(logger('dev'));

// Cookies / Session
var cookieParser = require('cookie-parser');
app.use(cookieParser());

// Favicon
// var favicon = require('serve-favicon');
//app.use(favicon(__dirname + '/public/favicon.ico'));

// View engine setup
app.locals.pretty = true;
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'public')));

// Disabling x-powered-by
app.disable('x-powered-by');

// Routes
require('./router')(app);

// Export application or start the server
if (!!module.parent) {
  module.exports = app;
} else {

  app.listen(deploy.get('PORT'), deploy.get('IP'), function () {
    console.log( 'Listening on ' + deploy.get('IP') + ', port ' + deploy.get('PORT') );
  });
}
