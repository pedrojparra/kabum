#Kabum

### Requisites

- Gulp Global >= 3.9.0
- Node >= 0.12.0

### Deploy
- rhc app create myapplication nodejs-0.12 NPM_CONFIG_PRODUCTION="true"
- rhc env-set NPM_CONFIG_PRODUCTION="true" --app <APP NAME>

### Run app

- npm install
- gulp build
- npm start

### Develop

- gulp
