/* jshint esnext: true, strict: true */
'use strict';
import gulp from 'gulp';
import gulpLoadPlugins from 'gulp-load-plugins';
import browserSync from 'browser-sync';
import nodemon from 'gulp-nodemon';
import del from 'del';

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

// Clean
gulp.task('clean', del.bind(null, [
  'app/public/fonts',
  'app/public/css'
]));

// Styles
gulp.task('styles', () => {
  return gulp.src('app/sass/main.sass')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['last 1 version']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('app/public/css'))
    .pipe(reload({stream: true}));
});

// Fonts
gulp.task('fonts', function() {
  return gulp.src(['app/bower_components/font-awesome/fonts/fontawesome-webfont.*'])
    .pipe(gulp.dest('app/public/fonts'));
});

// browser-sync
gulp.task('browser-sync', () => {

  browserSync.init({
    files: ['public/**/*.*'],
    proxy: 'http://localhost:8080',
    port: 4000,
  });

});

// Nodemon
gulp.task('nodemon', function () {
  nodemon({
    script:'app/app.js',
    ext: 'js jade',
  })
  .on('restart', () => {
    setTimeout( () => {
      reload({ stream: false });
    }, 500);
  });

});

gulp.task('default', ['clean','fonts','styles','browser-sync','nodemon'], ()=>{
  gulp.watch('app/sass/*.sass', ['styles']);
});
